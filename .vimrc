set number
set background=dark
syntax on
set expandtab
set tabstop=4
autocmd fileType tex setlocal spell spelllang=en_us
colors molokai
