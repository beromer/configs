PS1='\[\033[00;33m\]\u \[\033[00;36m\]@ \[\033[00;33m\]\h:\w \[\033[00;36m\]$\[\033[00m\] '
EDITOR='vim'
alias ls='ls --color=auto -alhFX --group-directories-first'
alias rm='rm -v'
alias cp='cp -v'
alias suspend="sudo pm-suspend"
alias hibernate="sudo pm-hibernate"
alias grep='grep --color=auto'
alias a='setxkbmap us'
alias aa='setxkbmap dvorak'
if [ -d "$HOME/bin" ]
then
    PATH=$PATH:$HOME/bin
fi
export PATH
export EDITOR

if [ -e "$HOME/.bashrc_local" ]
then
    echo "Incliding local .bachrc overrides"
    . $HOME/.bashrc_local
fi
