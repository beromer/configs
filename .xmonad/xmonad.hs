import XMonad
import XMonad.Layout.Spacing
import XMonad.Layout.NoBorders
import XMonad.Layout.PerWorkspace
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run
import System.IO

main = do
  xmproc <- spawnPipe "xmobar ~/.xmonad/xmobarrc"
  xmonad $ defaultConfig
    { borderWidth = myBorderWidth
    , modMask = myModMask
    , layoutHook = myLayout
    , terminal = myTerminal
    , workspaces = myWorkspaces
    , manageHook = manageDocks <+> manageHook defaultConfig
    , logHook = dynamicLogWithPP xmobarPP
        { ppOutput = hPutStrLn xmproc
        , ppTitle = xmobarColor "blue" "" . shorten 50
        , ppCurrent = xmobarColor "green" ""
        , ppHiddenNoWindows = xmobarColor "white" ""
        , ppHidden = xmobarColor "yellow" ""
        , ppOrder = \(wss:_:_:_) -> [wss]
        }
    }

myBorderWidth = 1
myModMask = mod4Mask
myTerminal = "urxvt"
--myWorkspaces = ["1:main","2:web","3:other","4:system"]
myWorkspaces = ["1","2","3","4"]
defaultLayouts = avoidStruts $ tiled ||| Mirror tiled ||| Full
    where
    tiled = spacing 5 $ Tall nmaster delta ratio
    tiles = Tall nmaster delta ratio
    nmaster = 1
    ratio = 2/3
    delta = 3/100
myLayout = onWorkspace "2" nobordersLayout $ defaultLayouts
    where
    nobordersLayout = noBorders $ Full

