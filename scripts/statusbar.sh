while true; do
    xsetroot -name "$(awk 'sub(/,/,"") {print $3, $4}' <(acpi -b)) | $( date +"%a, %b %d %I:%M %p" )"
    sleep 15
done
